# Problem Set 2: AppRewarder Functional web app



## Setup
The first thing to do is to clone the repository:

```
$ git clone https://gitlab.com/lakshminarayanamanchuri/apprewarder.git
$ cd apprewarder
```

Create a virtual environment to install dependencies:
```
$ python -m venv venv
$ venv/scripts/activate
(venv)$ pip install -r requirements.txt
```

Once pip has finished downloading the dependencies run the server:
```
(venv)$ python manage.py runserver
```
And navigate to http://127.0.0.1:8000

# Problem Set 1: Regex solution

## Solution:


```
import re

text = '''
{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}
'''

#pattern that matches "string": number pairs and standalone numbers
pattern = r'"([^"]+)":\s*(\d+)'

# Find all matches using the pattern
matches = re.findall(pattern, text)

# Extract integers from matches
integer_values = [int(value) for (_, value) in matches]

print(integer_values)
```

## Output:
```
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 648, 649, 650, 651, 652, 653, 3]
```

# Problem Set 3: 

## 3A Solution: System to schedule periodic tasks

For Periodic Tasks in django we can use "Celery" and "Celery Beat" Combined with a message broker like "Redis" or "RabbitMQ".

The Celery Beat will asign the task to the Celery periodically which will perfrom the taks outside our djagno server which will reduce the overhead on our website.

Celery is very reliable and Flexible define complex workflows with various scheduling options. It is also very scalable

Celery also offers various Features like task retries, task prioritization

### Problems
1. When the Task broker are not configured properly, if load becomes two high monitoring and scaling the broker infrastructure is crucial.

2. When Tasks fail Celery retries by default, but sometimes the tasks may fail due to various reasons we need to handle it gracefully.

### Problems at scaling in production fixes:

1. We can use "Load Balancing", "Distributed Brokers" to distribute tasks evenly and handle high message Volumes.

2. Have a scaling strategy in place to add more worker nodes and broker nodes when needed.

3. Implementing error handling and reporting to address task failures promptly.



## 3B Solution: Django and Flask
The choice between Djagno and Flask depends on the use case and specific requirements of the project.

### Django 
Django can be choosed when you project has these requirements.

1. Quick development: Django has wide range of built-in features like authentication, admin panel, and an ORM. It's ideal for rapid development when you want to get a project up and running quickly.

2. ORM and Database Integration: Django's ORM simplifies database management and migrations.

3. Complex Web Applications: If you are building Large scale complex applications, Django's structure and conventions can simplify development and maintenance.

4. Security: Django has built-in security features like protection against common web vulnerabilities (CSRF, XSS), making it a secure choice for web development.

### Flask

Flask can be choosed when you project has these requirements.

1. Microservices: Flask is a microframework that provides the essentials for building web applications. It's a great choice for small to medium-sized projects.

2. Flexibility: Flask is highly flexible and allows you to choose your libraries and components for tasks like database integration, authentication, and form handling. This flexibility is beneficial when you have specific requirements.

3. API Development: Its simplicity and minimalism make it well-suited for API development.

4. Customization: You have the freedom and flexibility to tweak and adjust your web application exactly the way you want it. 


So, Based on the above points 

If I want small to medium-sized projects, microservices, API development, flexibility, and when you need to customize extensively I would choose Flask.

And for rapid development, complex web applications, security, ORM/database integration, a admin interface I will choose Django
***

# Deployment of This Project:

For deploying my web application with an API, I chose PythonAnywhere because it is one of the best choices for hosting Django projects.

1. Cloned my project on to my PythonAnywhere account 
```
$ git clone https://gitlab.com/lakshminarayanamanchuri/apprewarder.git
```

2. Created virtual env and installed the requirements form the project applied.
```
$ cd apprewarder
$ python -m venv venv
$ source venv/bin/activate
(venv)$ pip install -r requirements.txt 
```

3. In the Web Deployment page set the path of the virtual env that you Created

4. In the settings page add your domain to allowed hosts
```
 ALLOWED_HOSTS = ['LakshmiNarayana.pythonanywhere.com']
 
```

5. configure your wsgi file for Deployment
```
import os
import sys

# assuming your django settings file is at '/home/LakshmiNarayana/mysite/mysite/settings.py'
# and your manage.py is is at '/home/LakshmiNarayana/mysite/manage.py'
path = '/home/LakshmiNarayana/apprewarder/AppRewarder'
if path not in sys.path:
    sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'AppRewarder.settings'

# then:
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
```

6. Then relaod the webpage from the Deployment page to reflect the changes and deploying the webapp.

Now, your web application should be successfully deployed on PythonAnywhere. Access it on this url https://lakshminarayana.pythonanywhere.com/

https://lakshminarayana.pythonanywhere.com/api endpoint for accessing the api's, used Token based authentication.

## Here is the short video link:

https://drive.google.com/file/d/1hI_0TgP6zzpovXLNFvUtMuc7L_FGbfWo/view?usp=sharing

